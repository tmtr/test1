﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GitRep.Startup))]
namespace GitRep
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
